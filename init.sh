#!/bin/sh

# Apply php.ini variables from environment
[ -n "${php_date__timezone}" ] || php_date__timezone="${processwire_timezone:-UTC}"
/usr/local/bin/apply-php-environment

# Rest of this file: setup everything for processwire

set -a -e
export $(cat /proc/self/environ | tr -d '\n' | tr '\0' '\n' | cut -d '=' -f 1)

# Default options
[ -n "${processwire_httpHosts}" ] || processwire_httpHosts="[$(hostname -f)]"

[ -n "${processwire_timezone}" ] || processwire_timezone="${php_date__timezone:-UTC}"
[ -n "${processwire_debug}" ] || processwire_debug="false"

[ -n "${processwire_dbHost}" ] || processwire_dbHost="${MYSQL_HOST:-database}"
[ -n "${processwire_dbPort}" ] || processwire_dbPort="${MYSQL_PORT:-3306}"
[ -n "${processwire_dbName}" ] || processwire_dbName="${MYSQL_DATABASE:-processwire}"
[ -n "${processwire_dbUser}" ] || processwire_dbUser="${MYSQL_USER:-root}"
[ -n "${processwire_dbPass}" ] || processwire_dbPass="${MYSQL_PASSWORD:-}"
[ -n "${processwire_dbCharset}" ] || processwire_dbCharset="${MYSQL_CHARSET:-utf8mb4}"
[ -n "${processwire_dbEngine}" ] || processwire_dbEngine="${MYSQL_ENGINE:-InnoDB}"

[ -n "${php_upload_max_filesize}" ] || php_upload_max_filesize=50M
[ -n "${php_post_max_size}" ] || php_post_max_size=55M

[ -n "${superuser_username}" ] || superuser_username='superuser'
[ -n "${superuser_email}" ] || superuser_email='superuser@example.email'

# Install & update ProcessWire
if ! [ -f composer.json ]; then
	rm -rf /opt/processwire
	composer create-project processwire/processwire /opt/processwire
	find /opt/processwire/ -mindepth 1 -maxdepth 1 -exec mv "{}" . "+"
	rmdir /opt/processwire
fi

# Install site profile & clean up
if ! [ -f site/assets/installed.php ]; then
	mkdir -p site/assets/{cache,logs,backups,sessions} site/modules
	find "${site_profile:-site-default}/" -mindepth 1 -maxdepth 1 -exec cp -r "{}" site/ "+"
	find "site/install/files/" -mindepth 1 -maxdepth 1 -exec cp -r "{}" site/assets/ "+"
	find "site-default/modules/" -mindepth 1 -maxdepth 1 -exec cp -r "{}" site/modules/ "+"
	# Install ProcessWireUpgrade module
	# TODO: disabled due to https://github.com/ryancramerdesign/ProcessWireUpgrade/issues/31
	#[ -d site/modules/ProcessWireUpgrade ] || git clone https://github.com/ryancramerdesign/ProcessWireUpgrade.git site/modules/ProcessWireUpgrade
fi
[ -e .htaccess ] || ln -s htaccess.txt .htaccess
[ -e site/.htaccess ] || ln -s htaccess.txt site/.htaccess

# Apply installation settings
sed -E -i ':a;N;$!ba;s/\n\n\/\*{3} ENVIRONMENT CONFIG \*{66}\/\n(.*|\n)*//' site/config.php

grep -q '^\$config->installed\s*=' site/config.php ||
  { echo; echo "\$config->installed = $(date +%s);"; } >> site/config.php

grep -q '^\$config->defaultAdminTheme\s*=' site/config.php ||
  echo "\$config->defaultAdminTheme = 'AdminThemeUikit';" >> site/config.php

grep -q '^\$config->chmodDir\s*=' site/config.php ||
  echo "\$config->chmodDir = '0755';" >> site/config.php

grep -q '^\$config->chmodFile\s*=' site/config.php ||
  echo "\$config->chmodFile = '0644';" >> site/config.php

grep -q '^\$config->userAuthSalt\s*=' site/config.php || [ -n "${processwire_userAuthSalt}" ] ||
  echo "\$config->userAuthSalt = '$(tr -dc 'a-f0-9' < /dev/urandom | head -c 40)';" >> site/config.php

grep -q '^\$config->tableSalt\s*=' site/config.php || [ -n "${processwire_tableSalt}" ] ||
  echo "\$config->tableSalt = '$(tr -dc 'a-f0-9' < /dev/urandom | head -c 40)';" >> site/config.php

# Apply configuration options
echo >> site/config.php
echo >> site/config.php
echo "/*** ENVIRONMENT CONFIG ******************************************************************/" >> site/config.php
echo "/*** Everything from here will be overwritten on every container restart! ****************/" >> site/config.php
echo >> site/config.php

for option in $(cat /proc/self/environ | tr -d '\n' | tr '\0' '\n' | cut -d '=' -f 1 | grep '^processwire_'); do
	value="$(printenv "$option")"
    option="$(echo "$option" | sed 's/^processwire_//')"
    if [ "$value" = "true" ] || [ "$value" = "false" ]; then
    	echo "\$config->${option} = $value;" >> site/config.php
	elif [[ "$value" == '['*']' ]]; then
		echo "\$config->${option} = explode(' ', '$(echo "$value" | sed -E -e 's/^\[\s*|\s*\]$//g' -e 's/\s+/ /g')');" >> site/config.php
	elif [[ "$value" == '<?'*'?>' ]]; then
		echo "\$config->${option} = $(echo "$value" | sed -E 's/^<\?(php|=)?\s*|\s*\?>$//g');" >> site/config.php
	else
		echo "\$config->${option} = '$value';" >> site/config.php
	fi
done

# Run installation script & clean up
if ! [ -f site/assets/installed.php ]; then
	status=2
	while [ $status -eq 2 ]; do
		php /opt/install.php && status=$? || status=$?
		[ $status -eq 0 ] || sleep 3
	done
	if [ $status -ne 0 ]; then exit $status; fi
	rm -rf site/install
fi

# Remove superfluous files
rm -rf install.php .gitignore $(printf "site-%s " beginner default languages blank regular classic)

# Ensure everything in /var/www belongs to the correct user
chown -R www-data:www-data /var/www
