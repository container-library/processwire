# ProcessWire Container Image
> A friendly and powerful open source CMS with an API that is a joy to use at any scale. (https://processwire.com/)

Container image for the ProcessWire CMS, based on our PHP image. Depends on MySQL/MariaDB.

Recreating the container (`docker-compose up -d --force-recreate`) will always update the ProcessWire Core to the latest stable version.

## Use it for your site with Docker Compose
Try the supplied `docker-compose.yml` file below to get started as quickly as possible - just run `docker-compose up -d`.

After starting the containers, you can reach the website at http://localhost:8080 and the admin interface at http://localhost:8080/processwire/.

```yaml
version: "3.8"
services:
  processwire:
    image: codeberg.org/container-library/processwire
    ports: [8080:80]
    hostname: www
    domainname: example.org
    volumes:
      - "./site:/var/www/html/site"
    environment:
      superuser_password: helloworld123
      #processwire_debug: "true"
  database:
    image: mariadb
    environment:
      MYSQL_ALLOW_EMPTY_PASSWORD: "true"
```

## Volumes
### `/var/www/html/site`
All the data required for your ProcessWire site.

## Environment Variables

### `site_profile`
Use a preset profile when creating a new site: `site-beginner`, `site-blank`, `site-classic`, `site-default`, `site-languages` or `site-regular`

### `superuser_username`, `superuser_password` & `superuser_email`
This sets the default superuser's credentials, defaulting to the username `superuser` with the email address `superuser@example.email`. `superuser_password` is required when creating a new site.

### `processwire_httpHosts`
Defaults to the container's FQDN hostname, but can be overwritten to allow further domains.

Note that this option is an array, so you have to use e.g. `processwire_httpHosts=[example.org www.example.org]`.

### `processwire_db*` and `MYSQL_*`
Database configuration variables:
- `processwire_dbHost`: MySQL/MariaDB host, default: `database` (alias: `MYSQL_HOST`)
- `processwire_dbPort`: MySQL/MariaDB port, default: `3306` (alias: `MYSQL_PORT`)
- `processwire_dbName`: MySQL/MariaDB database, default: `processwire` (alias: `MYSQL_DATABASE`)
- `processwire_dbUser`: MySQL/MariaDB user, default: `root` (alias: `MYSQL_USER`)
- `processwire_dbPass`: MySQL/MariaDB password, default: *empty* (alias: `MYSQL_PASSWORD`)
- `processwire_dbCharset`: `utf8` or `utf8mb4` (default)
- `processwire_dbEngine`: `MyISAM` or `InnoDB` (default)

### `processwire_*`
Set arbitrary ProcessWire configuration options like `processwire_debug=true`. You can find the full list of options [in the ProcessWire documentation](https://processwire.com/api/ref/config/).

You can use `true` or `false` for boolean values, `[string1 string2 ...]` for arrays, or other values for strings. For numbers or other PHP values, use `<? yourCodeHere`, for example `processwire_fatalErrorCode=<? 503`.

### `php_*`
Set arbitrary [php.ini options](https://www.php.net/manual/ini.list.php) like `php_sendmail_from=noreply@example.org`. More information on email and timezone setup can be found in the [php image](https://codeberg.org/container-library/php#modifying-php-ini).
