FROM codeberg.org/container-library/php

ADD init.sh /etc/
ADD install.php /opt/

VOLUME /var/www/html/site
