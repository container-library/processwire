<?php namespace ProcessWire;

// This file sets up the database & admin user, and is based on the official web-based installer:
// https://github.com/processwire/processwire/blob/master/install.php

// ProcessWire 3.x, Copyright 2019 by Ryan Cramer
// Mozilla Public License (MPL) Version 2.0
// https://processwire.com

error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_WARNING);

echo("Installing site...\n");
define("PROCESSWIRE", true);

// create database
$config = (object) array();
include("site/config.php");
$dsn = "mysql:dbname=" . $config->dbName . ";host=" . $config->dbHost . ";port=" . $config->dbPort;
$driver_options = array(
	\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
	\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
);
try {
	$database = new \PDO($dsn, $config->dbUser, $config->dbPass, $driver_options);
	echo("Database exists.\n");
} catch (\Exception $e) {	
	if ($e->getCode() == 1049) {
		// If schema does not exist, try to create it
		echo("Creating database...\n");
		$dbCharset = preg_replace('/[^a-z0-9]/', '', strtolower(substr($config->dbCharset, 0, 64)));
		$dbName = preg_replace('/[^_a-zA-Z0-9]/', '', substr($config->dbName, 0, 64));
		$dbNameTest = str_replace('_', '', $dbName);
		if (ctype_alnum($dbNameTest) && $dbName === $config->dbName
			&& ctype_alnum($dbCharset) && $dbCharset === $config->dbCharset) {
			// valid database name with no changes after sanitization
			try {
				$dsn2 = "mysql:host=" . $config->dbHost . ";port=" . $config->dbPort;
				$database = new \PDO($dsn2, $config->dbUser, $config->dbPass, $driver_options);
				$database->exec("CREATE SCHEMA IF NOT EXISTS `$dbName` DEFAULT CHARACTER SET `$dbCharset`");
				// reconnect
				$database = new \PDO($dsn, $config->dbUser, $config->dbPass, $driver_options);
				if ($database) echo("Created database: $dbName\n");
			} catch (\Exception $e) {
				echo("Failed to create database with name $dbName. " . $e->getMessage() . "\n");
				exit(1);
			}
		} else {
			echo("Unable to create database with that name. Please create the database with another tool and try again.\n"); 
			exit(1);
		}
	} else {
		echo("Database connection information did not work. " . $e->getMessage() . "\n");
		exit($e->getCode() == 2002 ? 2 : 1); // connection refused → code 2: wait & try again
	}
}

// migrate database
try {
	$query = $database->prepare("SHOW COLUMNS FROM pages"); 
	$result = $query->execute();
	echo("Database is already initialized.\n");
} catch (\Exception $e) {
	if ($e->getCode() != "42S02") {
		echo("Unable to check database state. " . $e->getMessage() . "\n");
		exit(1);
	}
	echo("Initializing database...\n");
	$restoreOptions = array();
	$replace = array();
	if ($config->dbEngine != 'MyISAM') {
		$replace['ENGINE=MyISAM'] = "ENGINE=" . $config->dbEngine;
	}
	if ($config->dbCharset != 'utf8') {
		$replace['CHARSET=utf8'] = "CHARSET=" . $config->dbCharset;
		if (strtolower($config->dbCharset) === 'utf8mb4') {
			if (strtolower($config->dbEngine) === 'innodb') {
				$replace['(255)'] = '(191)'; 
				$replace['(250)'] = '(191)'; 
			} else {
				$replace['(255)'] = '(250)'; // max ley length in utf8mb4 is 1000 (250 * 4)
			}
		}
	}
	if (count($replace)) $restoreOptions['findReplaceCreateTable'] = $replace; 
	require("./wire/core/WireDatabaseBackup.php"); 
	$backup = new WireDatabaseBackup(); 
	$backup->setDatabase($database);
	if (!$backup->restoreMerge("./wire/core/install.sql", "./site/install/install.sql", $restoreOptions)) {
		foreach($backup->errors() as $error) echo($error . "\n");
		exit(1);
	}
}

// load ProcessWire
unset($config);
include("index.php");

// create admin account
if (getenv("superuser_password") === false || getenv("superuser_password") == "") {
	echo("You must specify \$superuser_password.\n"); 
	exit(1);
}
echo("Creating superuser...\n");
$superuserRole = $wire->roles->get("name=superuser");
$user = $wire->users->get($wire->config->superUserPageID); 
if (!$user->id) {
	$user = new User();
	$user->id = $wire->config->superUserPageID;
}
$user->name = getenv("superuser_username");
$user->pass = getenv("superuser_password"); 
$user->email = getenv("superuser_email");
$user->admin_theme = $wire->modules->getInstall($wire->config->defaultAdminTheme);
if (!$user->roles->has("superuser")) $user->roles->add($superuserRole);
try {
	$wire->users->save($user); 
} catch (\Exception $e) {
	echo($e->getMessage() . "\n"); 
	exit(1);
}

// install ProcessWireUpgrade
//$wire->modules->install("ProcessWireUpgrade");
//$data = $modules->getConfig("ProcessWireUpgradeCheck");
//$data["useLoginHook"] = 1;
//$modules->saveConfig("ProcessWireUpgradeCheck", $data);

file_put_contents("./site/assets/installed.php", "<?php // The existence of this file prevents the installer from running. Don't delete it unless you want to re-run the install or you have deleted ./install.php."); 
